/*
 * config.h
 *
 *  Created on: 2023年1月8日
 *      Author: wangt
 */

#ifndef APPLICATION_HART1_CONFIG_H_
#define APPLICATION_HART1_CONFIG_H_

#define DILITHIUM_MODE 5

#define CRYPTO_ALGNAME "Dilithium5"
#define DILITHIUM_NAMESPACE(s) pqcrystals_dilithium5_ref##s

#endif /* APPLICATION_HART1_CONFIG_H_ */
